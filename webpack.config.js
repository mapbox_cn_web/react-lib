var path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var libraryName = "react-lib";

console.log("env", process.env.NODE_ENV);

module.exports = {
  entry: "./src/index.js",
  optimization: {
    minimize: true
  },
  target: "node",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    library: libraryName,
    libraryTarget: "umd",
    publicPath: "/dist/",
    umdNamedDefine: true
  },
  resolve: {
    extensions: [".js", ".jsx", ".scss"],
    alias: {
      components: path.resolve(__dirname, "src/components"),
      scss: path.resolve(__dirname, "src/assets/scss"),
      assets: path.resolve(__dirname, "src/assets"),
      data: path.resolve(__dirname, "src/data"),
      hocs: path.resolve(__dirname, "src/hocs"),
      utils: path.resolve(__dirname, "src/utils"),
      context: path.resolve(__dirname, "src/context"),
      node: path.resolve(__dirname, "node_modules")
    }
  },
  module: {
    rules: [
      {
        test: /\.(png|jpg|svg)$/,
        include: path.resolve(__dirname, "src"),
        use: "url-loader?name=images/[name].[ext]"
      },
      {
        test: /\.(woff|woff2|ttf|eot)$/,
        use: "file-loader?name=fonts/[name].[ext]!static"
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, "src"),
        loaders:
          process.env.NODE_ENV === "production"
            ? [MiniCssExtractPlugin.loader, "css-loader"]
            : ["style-loader", "css-loader"]
      },
      {
        test: /\.(scss|sass)$/,
        exclude: /\.module.(scss|sass)$/,
        include: path.resolve(__dirname, "src"),
        loaders:
          process.env.NODE_ENV === "production"
            ? [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
            : ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.module.(scss|sass)$/,
        include: path.resolve(__dirname, "src"),
        loaders:
          process.env.NODE_ENV === "production"
            ? [
                MiniCssExtractPlugin.loader,
                {
                  loader: "css-loader",
                  options: {
                    modules: true,
                    localIdentName: "[name]__[local]__[hash:base64:5]"
                  }
                },
                "sass-loader"
              ]
            : [
                "style-loader",
                {
                  loader: "css-loader",
                  options: {
                    modules: true,
                    localIdentName: "[name]__[local]__[hash:base64:5]"
                  }
                },
                "sass-loader"
              ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename:
        process.env.NODE_ENV === "production"
          ? "[name].css"
          : "[name].[hash].css",
      chunkFilename:
        process.env.NODE_ENV === "production" ? "[id].css" : "[id].[hash].css"
    })
  ]
};
