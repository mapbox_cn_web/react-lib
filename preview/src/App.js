import React, { Component } from "react";
import { Navi } from "react-lib";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navi />
      </div>
    );
  }
}

export default App;
