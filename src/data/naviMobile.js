module.exports = {
  products: {
    title: '产品',
    items: [
      { title: '地图', href: '/product-maps' },
      { title: '导航', href: '/product-navigation' },
      { title: '定位', href: '/product-search' },
      { title: '工作室', href: '/product-studio' },
    ],
  },
  platforms: {
    title: '平台',
    items: [
      { title: '移动端', href: '/product-mobile' },
      { title: 'AR', href: '/product-ar' },
      { title: '汽车平台', href: '/product-auto' },
      { title: '网页端', href: '/tutorials/gljs' },
    ],
  },
  company: {
    title: '公司',
    items: [
      { title: '关于我们', href: '/about' },
      { title: '博客', href: '/community-blog' },
      { title: '活动', href: '/events' },
      { title: '联系我们', href: '/contact-us' },
    ],
  },
  developers: {
    title: '开发者资源',
    items: [
      { title: '开发文档', href: '/documentation' },
      { title: '示例', href: '/examples' },
      { title: '教程', href: '/tutorials' },
    ],
  },
}
