import React from "react";
import PropTypes from "prop-types";

export default function platformHoc(WrappedComponent, width = 768) {
  return class extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        isMobile: false
      };
    }

    componentDidMount() {
      this.window = window;
      window.addEventListener("resize", this.handleResize);
      this.handleResize();
    }

    handleResize = () => {
      if (!this.mobile && this.window.innerWidth <= width) {
        this.mobile = true;
        this.setState({ isMobile: true });
      } else if (this.mobile && this.window.innerWidth > width) {
        this.mobile = false;
        this.setState({ isMobile: false });
      }
    };

    render() {
      return (
        <WrappedComponent isMobile={this.state.isMobile} {...this.props} />
      );
    }
  };
}
