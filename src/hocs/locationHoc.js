import React from "react";
import PropTypes from "prop-types";

export default function locationHoc(WrappedComponent) {
  return class extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        location: {
          hash: "",
          host: "",
          href: ""
        }
      };
    }

    componentDidMount() {
      this.setState({
        location: {
          hash: window.location.hash,
          host: window.location.host,
          href: window.location.href
        }
      });
    }

    render() {
      return (
        <WrappedComponent location={this.state.location} {...this.props} />
      );
    }
  };
}
