import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import styles from './Popover.module.scss'

export default class Popover extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.any,
    styleWrapper: PropTypes.object,
    align: PropTypes.string
  }

  static defaultProps = {
    className: '',
    style: {},
    children: null,
    styleWrapper: {},
    align: 'bottom'
  }

  render() {
    const { style, className, children, styleWrapper, align } = this.props

    return (
      <div
        className={`${styles.root} ${className} ${styles[align]}`}
        style={style}
      >
        <div className={`${styles.triangle}`} />
        <div className={`${styles.wrapper}`} style={styleWrapper}>
          {children}
        </div>
      </div>
    )
  }
}
