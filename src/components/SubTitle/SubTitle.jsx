import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Text from "components/Text";
import styles from "./SubTitle.module.scss";

class SubTitle extends PureComponent {
  render() {
    const { className, title, sup, href } = this.props;

    return (
      <div className={classnames(styles.root, className)}>
        <Text fontSize="medium_l" fontWeight="semibold">
          <a className={styles.sub} href={href}>
            {sup}
          </a>
        </Text>
        <Text fontSize="large" fontWeight="semibold">
          {title}
        </Text>
      </div>
    );
  }
}

SubTitle.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  title: PropTypes.string,
  sup: PropTypes.string
};

SubTitle.defaultProps = {
  href: "/"
};

export default SubTitle;
