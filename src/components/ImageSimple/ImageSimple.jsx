import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

export default class ImageSimple extends PureComponent {
  static propTypes = {
    path: PropTypes.string.isRequired,
    alt: PropTypes.string,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    className: PropTypes.string,
    style: PropTypes.object,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    alt: '',
    width: '100%',
    height: '100%',
    className: null,
    style: {},
    onClick: () => {},
  }

  render() {
    const { className, path, width, height, alt, style, onClick } = this.props
    const src = require(`assets/img/${path}`)

    return (
      <img
        className={className}
        width={Number.isInteger(width) ? `${width}px` : width}
        height={Number.isInteger(height) ? `${height}px` : height}
        src={src}
        alt={alt}
        className={className}
        style={style}
        onClick={onClick}
      />
    )
  }
}
