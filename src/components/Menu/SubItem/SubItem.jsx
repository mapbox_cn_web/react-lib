import React, { PureComponent } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import Text from "components/Text";
import styles from "./SubItem.module.scss";

class SubItem extends PureComponent {
  componentDidMount() {
    this.location = window.location;
  }

  handleClick = () => {
    this.location.hash = this.props.slug;
  };

  render() {
    const { className, path, text, slug, isOpen } = this.props;

    return (
      <div
        className={classnames(
          styles.root,
          className,
          isOpen ? styles.open : ""
        )}
        onClick={this.handleClick}
      >
        <Text fontSize="normal" color="secondary" fontWeight="semibold">
          {text}
        </Text>
      </div>
    );
  }
}

SubItem.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  path: PropTypes.string,
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  onClick: PropTypes.func
};

SubItem.defaultProps = {
  items: [],
  onClick: () => {}
};

export default SubItem;
