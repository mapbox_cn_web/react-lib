import React, { PureComponent } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import SvgIcon from "components/SvgIcon";
import Text from "components/Text";
import SubItem from "../SubItem";
import styles from "./Item.module.scss";

class Item extends PureComponent {
  handleClick = () => {
    const { id, onItemClick } = this.props;

    onItemClick(id);
  };

  render() {
    const { className, path, title, items, isOpen } = this.props;

    return (
      <div
        className={classnames(
          styles.root,
          className,
          isOpen ? styles.open : ""
        )}
      >
        <a href={path}>
          <Text
            className={classnames(styles.name)}
            fontSize="normal"
            fontWeight="semibold"
            onClick={this.handleClick}
          >
            {title}
          </Text>
          <SvgIcon className={styles.arrow} width={6} name="miniArrow" />
        </a>
        {isOpen &&
          items.map((item, index) => (
            <SubItem key={index} id={index} path={path} {...item} />
          ))}
      </div>
    );
  }
}

Item.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  items: PropTypes.array,
  isOpen: PropTypes.bool,
  onClick: PropTypes.func
};

Item.defaultProps = {
  items: [],
  current: "",
  onClick: () => {}
};

export default Item;
