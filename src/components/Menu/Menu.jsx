import React, { PureComponent } from "react";
import find from "lodash/find";
import classnames from "classnames";
import PropTypes from "prop-types";

import Item from "./Item";
import styles from "./Menu.module.scss";

class Menu extends PureComponent {
  render() {
    const { className, currentPath, content, children } = this.props;
    const { firstLevelToc, secondLevelToc } = content;

    return (
      <div className={classnames(styles.root, className)}>
        <div className={styles.wrapper}>
          <div className={styles.menu}>
            {firstLevelToc.map(item => (
              <Item
                {...item}
                key={item.path}
                items={secondLevelToc}
                isOpen={item.path === currentPath}
              />
            ))}
          </div>
        </div>
        <div className={styles.children}>{children}</div>
      </div>
    );
  }
}

Menu.propTypes = {
  className: PropTypes.string,
  content: PropTypes.object,
  currentPath: PropTypes.string
};

Menu.defaultProps = {
  className: "",
  content: { firstLevelToc: [], secondLevelToc: [] },
  currentPath: ""
};

export default Menu;
