import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import { timeShape, easingShape } from './propTypes'

class Animation extends PureComponent {
  constructor(props) {
    super(props)
    this.state = props.animateOnMount ? this.getAnimatedState(props) : {}
  }

  componentWillReceiveProps(nextProps) {
    const { isVisible } = nextProps

    if (isVisible !== this.props.isVisible) {
      this.setState(this.getAnimatedState(nextProps))
    }
  }

  getAnimatedState = props => {
    const { isVisible, duration, easing, delay } = props

    const type = isVisible ? 'in' : 'out'

    return {
      animation: isVisible ? this.props.in : this.props.out,
      delay: delay instanceof Object ? delay[type] : delay,
      easing: easing instanceof Object ? easing[type] : easing,
      duration: duration instanceof Object ? duration[type] : duration
    }
  }

  render() {
    const {
      style,
      className,
      children,
      innerRef,
      isVisible,
      onClick
    } = this.props

    const { delay, easing, duration, animation } = this.state

    style.opacity = animation ? null : Number(isVisible)

    return (
      <div
        className={className}
        ref={innerRef}
        style={{
          animationName: animation,
          animationDelay: `${delay}ms`,
          animationTimingFunction: easing,
          animationDuration: `${duration}ms`,
          pointerEvents: isVisible ? 'all' : 'none',
          animationFillMode: 'both',
          ...style
        }}
        onClick={onClick}
      >
        {children}
      </div>
    )
  }
}

/* eslint-disable react/no-unused-prop-types */
Animation.propTypes = {
  className: PropTypes.string,
  innerRef: PropTypes.func,
  style: PropTypes.object,
  isVisible: PropTypes.bool,
  in: PropTypes.string,
  out: PropTypes.string,
  delay: timeShape,
  duration: timeShape,
  easing: easingShape,
  animateOnMount: PropTypes.bool,
  children: PropTypes.any,
  onClick: PropTypes.func
}
/* eslint-enable */

Animation.defaultProps = {
  style: {},
  isVisible: true,
  in: 'fadeIn',
  out: 'fadeOut',
  delay: 0,
  duration: 300,
  easing: 'ease',
  animateOnMount: false,
  onClick: () => {}
}

export default Animation
