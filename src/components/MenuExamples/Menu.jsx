import React, { PureComponent } from "react";
import find from "lodash/find";
import classnames from "classnames";
import PropTypes from "prop-types";

import Item from "./Item";
import styles from "./Menu.module.scss";

const findCurrentByPathname = (pathname, items) => {
  for (let i = 0; i < items.length; i++) {
    if (find(items[i].items, { path: pathname })) {
      return items[i].title;
    }
  }

  return "";
};

const getItems = content => {
  let items = [];
  const { secondLevelToc } = content;

  for (let i = 0; i < secondLevelToc.length; i++) {
    const find = false;

    for (let j = 0; j < items.length; j++) {
      if (secondLevelToc[i].topic === items[j].title) {
        items[j].items.push(secondLevelToc[i]);
        find = true;
      }
    }

    if (!find) {
      items.push({
        title: secondLevelToc[i].topic,
        items: [secondLevelToc[i]]
      });
    }
  }

  return items;
};

class Menu extends PureComponent {
  state = {
    current: ""
  };

  componentDidMount() {
    const pathname = window.location.pathname;
    const items = getItems(this.props.content);
    const current = findCurrentByPathname(pathname, items);

    this.setState({ current });
  }

  handleItemClick = current => {
    this.setState({ current });
  };

  render() {
    const { className, content, children } = this.props;
    const { current } = this.state;
    const items = getItems(content);

    return (
      <div className={classnames(styles.root, className)}>
        <div className={styles.wrapper}>
          <div className={styles.menu}>
            {items.map(item => (
              <Item
                {...item}
                key={item.path}
                items={item.items}
                isOpen={item.title === current}
                onItemClick={this.handleItemClick}
              />
            ))}
          </div>
        </div>
        <div className={styles.children}>{children}</div>
      </div>
    );
  }
}

Menu.propTypes = {
  className: PropTypes.string,
  content: PropTypes.object
};

Menu.defaultProps = {
  className: "",
  content: { firstLevelToc: [], secondLevelToc: [] }
};

export default Menu;
