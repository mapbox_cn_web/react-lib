import React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import markers from './icons/markers'
import brands from './icons/brands'
import numbers from './icons/numbers'
import nav from './icons/nav'
import panda from './icons/panda'
import mini from './icons/mini'
import development from './icons/development'
import buildingBlock from './icons/buildingBlock'
import fly from './icons/fly'
import bg from './icons/bg'
import features from './icons/features'
import actions from './icons/actions'
import common from './icons/common'

/* eslint-disable max-len */
const sprites = {
  ...fly,
  ...markers,
  ...brands,
  ...numbers,
  ...nav,
  ...panda,
  ...mini,
  ...development,
  ...buildingBlock,
  ...bg,
  ...features,
  ...actions,
  ...common,
}

export default sprites
