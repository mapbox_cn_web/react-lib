import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Animation from "components/Animation";
import SvgIcon from "components/SvgIcon";
import Text from "components/Text";
import Button from "components/Button";
import styles from "./CustomModal.module.scss";

export default class CustomModal extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    onEnter: PropTypes.func,
    onLeave: PropTypes.func
  };

  static defaultProps = {
    className: "",
    open: false,
    onEnter: () => {},
    onLeave: () => {}
  };

  handleInnerClick = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  render() {
    const { className, open, onEnter, onLeave } = this.props;

    return (
      <Animation
        isVisible={open}
        in={styles.modalAnimationVisible}
        out={styles.modalAnimationHidden}
        duration={{ in: 300, out: 300 }}
        className={`${styles.root} ${open ? styles.open : ""}`}
        onClick={onLeave}
      >
        <Animation
          isVisible={open}
          in={styles.modalAnimationIn}
          out={styles.modalAnimationOut}
          duration={{ in: 300, out: 300 }}
          className={styles.inner}
          onClick={this.handleInnerClick}
        >
          <button className={styles.cross} onClick={onLeave}>
            <SvgIcon name="cross" height="26px" />
          </button>
          <SvgIcon name="pandaPopup" height="176px" />
          <Text
            tag="h3"
            fontSize="medium_xl"
            fontWeight="semibold"
            textAlign="center"
            color="primary"
            lineHeight="normal"
            className={styles.title}
          >
            你正要离开Mapbox中国网站
          </Text>
          <Text
            fontSize="small"
            textAlign="center"
            color="secondary"
            lineHeight="medium_l"
            className={styles.description}
          >
            并非所有mapbox.com的服务在中国提供
          </Text>
          <div className={styles.actions}>
            <Button
              label="前往国际网站"
              theme="gradientInvert"
              onClick={onEnter}
            />
            <Button label="暂时不去" theme="borderBlue" onClick={onLeave} />
          </div>
        </Animation>
      </Animation>
    );
  }
}
