import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ImageSimple from 'components/ImageSimple'

export default class Image extends PureComponent {
  static propTypes = {
    path: PropTypes.string.isRequired,
    alt: PropTypes.string,
    className: PropTypes.string,
    classNamePicture: PropTypes.string,
    style: PropTypes.object,
  }

  static defaultProps = {
    alt: '',
    className: null,
    classNamePicture: '',
    style: null,
  }

  render() {
    const {
      path,
      alt,
      style,
      className,
      classNamePicture,
      ...otherProps
    } = this.props

    const type = path.split('.')[1]

    if (type == 'svg') {
      return <ImageSimple {...this.props} />
    }

    if (path.search('assets') !== -1) {
      console.log('Дан неверный путь к картинке', path)
      return <div />
    } else {
      const src = require(`assets/img/${path}`)
      const srcX2 = require(`assets/img/${path.split('.').join('-x2.')}`)

      return (
        <picture className={classNamePicture}>
          <source srcSet={srcX2 ? srcX2 : src} media="(min-width: 1280px)" />
          <img
            {...otherProps}
            src={src}
            alt={alt}
            className={className}
            style={style}
          />
        </picture>
      )
    }
  }
}
