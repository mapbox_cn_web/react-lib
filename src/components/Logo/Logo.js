import React, { PureComponent } from 'react'
import classnames from 'classnames'
import styles from './Logo.module.scss'
import PropTypes from 'prop-types'
import SvgIcon from 'components/SvgIcon'

export default class Logo extends PureComponent {
  static propTypes = {
    type: PropTypes.oneOf(['mobile', 'desktop']),
    isTransparent: PropTypes.bool,
    withTitle: PropTypes.bool,
    width: PropTypes.number
  }

  static defaultProps = {
    type: 'desktop',
    isTransparent: false,
    withTitle: true
  }

  getIconWidth = type => {
    switch (type) {
      case 'mobile':
        return 126
      default:
        return 158
    }
  }

  getSuperscriptHeight = type => {
    switch (type) {
      case 'mobile':
        return 12
      default:
        return 15
    }
  }

  render() {
    const { type, isTransparent, withTitle, width } = this.props
    const rootClass = !isTransparent
      ? styles.logo
      : `${styles.logo} ${styles.transparent}`
    const titleClass = withTitle ? '' : styles.withTitle
    const svgClass = isTransparent ? styles.svgGray : styles.svgBlue
    const iconWidth = this.getIconWidth(type)
    const superscriptHeight = this.getSuperscriptHeight(type)

    return (
      <a href='/'>
        <h1
          className={classnames(styles.root, {
            [styles.transparent]: isTransparent,
            [styles.withTitle]: withTitle,
            [styles[type]]: type
          })}
        >
          <SvgIcon
            name='logo'
            className={classnames(styles.icon)}
            width={iconWidth}
          />
          <span className={styles.superscript}>
            <SvgIcon name='logoHie' height={superscriptHeight} />
          </span>
        </h1>
      </a>
    )
  }
}
