import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { mobileWidth } from "data/breakpoints";
import Logo from "components/Logo";
import DesktopNavi from "./DesktopNavi";
import TabletNavi from "./TabletNavi";
import Burger from "./Burger";

import { PopupContext } from "context";
import CustomModal from "components/CustomModal";

import styles from "./Navi.module.scss";

// import 'animate.css/animate.css'
// import 'font-awesome/css/font-awesome.css'

const mobileWidthCheck =
  typeof window !== "undefined" && window.matchMedia
    ? window.matchMedia(`(max-width: ${mobileWidth}px)`)
    : {};

export default class Navi extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      onAway: this.handleAway,
      path: undefined,
      isMobile: false,
      status: false,
      resolution: "desktop",
      open: false,
      onAway: this.handleAway,
      path: undefined
    };
  }

  static propTypes = {
    isTransparent: PropTypes.bool
  };

  static defaultProps = {
    isTransparent: false
  };

  componentDidMount() {
    this.window = window;
    window.addEventListener("resize", this.handleResize);
    mobileWidthCheck.addListener(this.setMobileLogo);

    this.setState({
      isMobile:
        typeof window !== "undefined" ? window.innerWidth <= 768 : false,
      resolution:
        typeof window !== "undefined" &&
        window.innerWidth &&
        window.innerWidth <= mobileWidth
          ? "mobile"
          : "desktop"
    });
  }

  // -- был баг с меню -- //
  //
  // componentDidUpdate() {
  //   const { status } = this.state
  //
  //   document.body.style.overflow = status ? 'hidden' : null
  //   document.documentElement.style.overflow = status ? 'hidden' : null
  // }

  componentWillUnmount() {
    this.window.removeEventListener("resize", this.handleResize);
    mobileWidthCheck.removeListener(this.setMobileLogo);
  }

  setMobileLogo = mediaQueryList => {
    const { matches } = mediaQueryList;

    this.setState({ resolution: matches ? "mobile" : "desktop" });
  };

  handleResize = () => {
    if (!this.mobile && this.window.innerWidth <= 769) {
      this.mobile = true;
      this.setState({ isMobile: true });
    } else if (this.mobile && this.window.innerWidth > 769) {
      this.mobile = false;
      this.setState({ isMobile: false });
    }
  };

  handleClick = () => {
    const { status } = this.state;

    this.setState({ status: !status });

    document.documentElement.style.overflow = !status ? "hidden" : null;
  };

  handleEnter = () => {
    this.setState({ open: false });
    window.open(this.state.href, "_blank");
  };

  handleLeave = e => {
    this.setState({ open: false });
  };

  handleAway = href => {
    console.log("OPEN");

    this.setState({ open: true, href });
  };

  render() {
    const { isTransparent } = this.props;

    const rootClass = !isTransparent
      ? styles.root
      : `${styles.root} ${styles.transparent}`;
    const { isMobile, status, resolution } = this.state;

    return (
      <PopupContext.Provider value={this.state}>
        <nav>
          <div className="container-fluid">
            <div className={rootClass}>
              <div className={styles.wrapper}>
                <Logo
                  width={158}
                  type={resolution}
                  withTitle
                  isTransparent={status === true ? false : isTransparent}
                />
                {isMobile && (
                  <Burger status={status} onClick={this.handleClick} />
                )}
                {!isMobile && (
                  <DesktopNavi isTransparent={isTransparent} pathname="" />
                )}
              </div>
            </div>
            <TabletNavi isTransparent={isTransparent} isVisible={status} />
          </div>
        </nav>

        <CustomModal
          open={this.state.open}
          onEnter={this.handleEnter}
          onLeave={this.handleLeave}
        />
      </PopupContext.Provider>
    );
  }
}
