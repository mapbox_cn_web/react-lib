import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import styles from './Burger.module.scss'

export default class Burger extends PureComponent {
  static propTypes = {
    status: PropTypes.bool,
    onClick: PropTypes.func.isRequired
  }

  render() {
    const { status, onClick } = this.props

    return (
      <button
        className={classnames(styles.root, {
          [styles.open]: status,
          [styles.close]: !status
        })}
        onClick={onClick}
      >
        <div className={styles.burgerWrapper}>
          <div className={classnames(styles.icon, styles.top)} />
          <div className={classnames(styles.icon, styles.middle)} />
          <div className={classnames(styles.icon, styles.bottom)} />
        </div>
      </button>
    )
  }
}
