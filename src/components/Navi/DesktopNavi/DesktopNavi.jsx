import React, { PureComponent, Fragment } from "react";
// import { Link, push } from 'gatsby'
import styles from "./DesktopNavi.module.scss";
import Button from "components/Button";
import PropTypes from "prop-types";

import navLinks from "data/navi";
import Hover from "components/Hover";
import Animation from "components/Animation";

import PopoverProducts from "./PopoverProducts";
import PopoverDocumentation from "./PopoverDocumentation";
import PopoverTutorials from "./PopoverTutorials";
import PopoverComunity from "./PopoverComunity";

const AnimationWrapper = ({ children, isVisible }) => (
  <Animation
    in={styles.showPopover}
    out={styles.hidePopover}
    duration={{ in: 300, out: 200 }}
    easing="cubic-bezier(0.165, 0.840, 0.440, 1.000)"
    isVisible={isVisible}
  >
    {children}
  </Animation>
);

export default class Navi extends PureComponent {
  static propTypes = {
    pathname: PropTypes.string.isRequired,
    isTransparent: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {};
    navLinks.forEach(item => {
      if (item.popover) {
        this.state[item.href] = false;
      }
    });
  }

  componentDidMount() {
    this.window = window;
  }

  handleSingIn = () => {
    this.window.open("/contact-us", "_self");
  };

  handleHover = pathname => isHover => {
    if (isHover) {
      this.setState({ [pathname]: true });
    } else {
      this.setState({ [pathname]: false });
    }
  };

  render() {
    const { pathname, isTransparent } = this.props;
    const rootClass = !isTransparent
      ? styles.root
      : `${styles.root} ${styles.transparent}`;

    return (
      <div className={rootClass}>
        <ul className={styles.nav}>
          {navLinks.map(item => (
            <Hover onHover={this.handleHover(item.href)} key={item.href}>
              <li
                key={item.href}
                className={
                  pathname === item.href
                    ? `${styles.navItem} ${styles.active}`
                    : styles.navItem
                }
              >
                {item.popover ? (
                  <Fragment>
                    <a href={item.href} className={styles.navLink}>
                      {item.name}
                    </a>
                    {item.href === "/products" && (
                      <AnimationWrapper isVisible={this.state[item.href]}>
                        <PopoverProducts data={item.popover} />
                      </AnimationWrapper>
                    )}
                    {item.href === "/documentation" && (
                      <AnimationWrapper isVisible={this.state[item.href]}>
                        <PopoverDocumentation data={item.popover} />
                      </AnimationWrapper>
                    )}
                    {item.href === "/tutorials" && (
                      <AnimationWrapper isVisible={this.state[item.href]}>
                        <PopoverTutorials data={item.popover} />
                      </AnimationWrapper>
                    )}
                    {item.href === "/community" && (
                      <AnimationWrapper isVisible={this.state[item.href]}>
                        <PopoverComunity
                          data={item.popover}
                          open={this.state[item.href]}
                        />
                      </AnimationWrapper>
                    )}
                  </Fragment>
                ) : (
                  <a href={item.href} className={styles.navLink}>
                    {item.name}
                  </a>
                )}
              </li>
            </Hover>
          ))}
        </ul>
        <Button
          label="联系我们"
          theme={isTransparent ? "transparent" : "default"}
          onClick={this.handleSingIn}
          className={styles.btnToken}
        />
      </div>
    );
  }
}
