import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import Text from 'components/Text'
import LinkTo from 'components/LinkTo'
import styles from './SideItem.module.scss'

class SideItem extends PureComponent {
  static propTypes = {
    link: PropTypes.string,
    title: PropTypes.string,
    desc: PropTypes.string,
  }

  render() {
    const { className, link, title, desc } = this.props

    return (
      <LinkTo href={link} className={classnames(styles.root, className)}>
        <Text fontSize="mini" color="primary" fontWeight="semibold">
          {title}
        </Text>
        <Text className={styles.desc} fontSize="mini" color="light">
          {desc}
        </Text>
      </LinkTo>
    )
  }
}

export default SideItem
