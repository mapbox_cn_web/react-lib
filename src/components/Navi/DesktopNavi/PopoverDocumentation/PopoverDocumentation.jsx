import React, { PureComponent } from "react";
// import { Link, push } from 'gatsby'
import styles from "./PopoverDocumentation.module.scss";

import PropTypes from "prop-types";

import Text from "components/Text";
import Popover from "components/Popover";
import LinkTo from "components/LinkTo";
import ButtonLink from "components/ButtonLink";
import SideItem from "./SideItem";

export default class PopoverDocumentation extends PureComponent {
  // handleClick = data => event => {
  //   if (data.target) {
  //     window.open(data.href)
  //   } else {
  //     // push(data.href)
  //   }
  // }

  render() {
    const { data } = this.props;

    return (
      <Popover
        style={{ marginLeft: 25 }}
        styleWrapper={{ width: 744, height: 322 }}
        className={styles.root}
      >
        <div className={`row ${styles.row}`}>
          <div className="col-11">
            <div className="row">
              <div className="col-4">
                <div className={styles.headingTitle}>{data.heading1}</div>
                <LinkTo
                  className={styles.navLink}
                  href={data.androidMapSdk.href}
                >
                  {data.androidMapSdk.name}
                </LinkTo>
                <LinkTo
                  className={styles.navLink}
                  href={data.androidNavigationSDK.href}
                >
                  {data.androidNavigationSDK.name}
                </LinkTo>
              </div>
              <div className="col-4">
                <div className={styles.headingTitle}>{data.heading2}</div>
                <LinkTo className={styles.navLink} href={data.iOSMapSDK.href}>
                  {data.iOSMapSDK.name}
                </LinkTo>
                <LinkTo
                  className={styles.navLink}
                  href={data.iOSNavigationSDK.href}
                >
                  {data.iOSNavigationSDK.name}
                </LinkTo>
              </div>
              <div className="col-4">
                <div className={styles.headingTitle}>{data.heading3}</div>
                <LinkTo className={styles.navLink} href={data.mapGLJS.href}>
                  {data.mapGLJS.name}
                </LinkTo>
              </div>
              <div className="col-4">
                <div className={styles.headingTitle}>{data.heading4}</div>
                <LinkTo className={styles.navLink} href={data.unitySDK.href}>
                  {data.unitySDK.name}
                </LinkTo>
                <LinkTo className={styles.navLink} href={data.qtSDK.href}>
                  {data.qtSDK.name}
                </LinkTo>
              </div>
              <div className="col-4">
                <div className={styles.headingTitle}>{data.heading5}</div>
                <LinkTo className={styles.navLink} href={data.apiMaps.href}>
                  {data.apiMaps.name}
                </LinkTo>
                <LinkTo
                  className={styles.navLink}
                  href={data.apiDirections.href}
                >
                  {data.apiDirections.name}
                </LinkTo>
                <LinkTo
                  className={styles.navLink}
                  href={data.apiGeocoding.href}
                >
                  {data.apiGeocoding.name}
                </LinkTo>
              </div>
            </div>
            <div className={styles.allDocs}>
              <div className={styles.navMore} href={data.allDocumentation.href}>
                <ButtonLink className={styles.more} link="/documentation">
                  {data.allDocumentation.name}
                </ButtonLink>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.side}>
          <Text color="light" fontSize="mini">
            辅助类文档和工具
          </Text>
          <div className={styles.line} />
          <SideItem
            className={styles.item}
            link="/examples"
            title="示例中心"
            desc="通过浏览示例以及程序代码了解如何使用Mapbox"
          />
          <SideItem
            className={styles.item}
            link="https://www.mapbox.com/help/interactive-tools/"
            title="地图工具"
            desc="一些帮助程序员开发Mapbox地图的小工具"
          />
        </div>
      </Popover>
    );
  }
}
