import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Animated from 'components/Animated'

import Items from './Items'
import styles from './TabletNavi.module.scss'
import data from 'data/naviMobile'

export default class TabletNavi extends PureComponent {
  static propTypes = {
    isVisible: PropTypes.bool
  }

  render() {
    const { isVisible } = this.props

    return (
      <Animated className={styles.root} isVisible={isVisible}>
        <div className={styles.wrapper}>
          <div className={`container-fluid ${styles.container}`}>
            <div className={`row`}>
              <div className={`col-6`}>
                <Items
                  title={data.products.title}
                  items={data.products.items}
                />
              </div>
              <div className={`col-6`}>
                <Items
                  title={data.platforms.title}
                  items={data.platforms.items}
                />
              </div>
            </div>
            <div className={styles.separator} />
            <div className={`row`}>
              <div className={`col-6`}>
                <Items title={data.company.title} items={data.company.items} />
              </div>
              <div className={`col-6`}>
                <Items
                  title={data.developers.title}
                  items={data.developers.items}
                />
              </div>
              <div />
            </div>
          </div>
        </div>
      </Animated>
    )
  }
}
