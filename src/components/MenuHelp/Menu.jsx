import React, { PureComponent } from "react";
import find from "lodash/find";
import classnames from "classnames";
import PropTypes from "prop-types";

import Item from "./Item";
import styles from "./Menu.module.scss";

class Menu extends PureComponent {
  state = {
    current: ""
  };

  handleItemClick = current => {
    this.setState({ current });
  };

  render() {
    const { className, content, children } = this.props;
    const { current } = this.state;

    return (
      <div className={classnames(styles.root, className)}>
        <div className={styles.wrapper}>
          <div className={styles.menu}>
            {content.map(item => (
              <Item
                {...item}
                key={item.path}
                items={item.guides}
                isOpen={item.title === current}
                onItemClick={this.handleItemClick}
              />
            ))}
          </div>
        </div>
        <div className={styles.children}>{children}</div>
      </div>
    );
  }
}

Menu.propTypes = {
  className: PropTypes.string,
  content: PropTypes.object
};

Menu.defaultProps = {
  className: "",
  content: { firstLevelToc: [], secondLevelToc: [] }
};

export default Menu;
