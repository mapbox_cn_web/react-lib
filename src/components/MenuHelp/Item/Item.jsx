import React, { PureComponent } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import SvgIcon from "components/SvgIcon";
import Text from "components/Text";
import SubItem from "../SubItem";
import styles from "./Item.module.scss";

class Item extends PureComponent {
  componentDidMount() {
    this.location = window.location;
  }

  handleClick = () => {
    const { title, onItemClick } = this.props;

    this.location.hash = this.props.path;
    onItemClick(title);
  };

  render() {
    const { className, title, items, isOpen } = this.props;

    return (
      <div
        className={classnames(
          styles.root,
          className,
          isOpen ? styles.open : ""
        )}
        onClick={this.handleClick}
      >
        <div>
          <Text
            className={classnames(styles.name)}
            fontSize="normal"
            fontWeight="semibold"
            onClick={this.handleClick}
          >
            {title} ({items.length})
          </Text>
          <SvgIcon className={styles.arrow} width={6} name="miniArrow" />
        </div>
        {isOpen &&
          items.map((item, index) => (
            <SubItem key={index} id={index} {...item} />
          ))}
      </div>
    );
  }
}

Item.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  items: PropTypes.array,
  isOpen: PropTypes.bool,
  onItemClick: PropTypes.func
};

Item.defaultProps = {
  items: [],
  current: "",
  onItemClick: () => {}
};

export default Item;
