import React, { PureComponent } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import Text from "components/Text";
import styles from "./SubItem.module.scss";

class SubItem extends PureComponent {
  render() {
    const { className, path, title, isOpen } = this.props;

    return (
      <a
        href={path}
        className={classnames(
          styles.root,
          className,
          isOpen ? styles.open : ""
        )}
      >
        <Text fontSize="normal" color="secondary" fontWeight="semibold">
          {title}
        </Text>
      </a>
    );
  }
}

SubItem.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  path: PropTypes.string,
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  onClick: PropTypes.func
};

SubItem.defaultProps = {
  items: [],
  onClick: () => {}
};

export default SubItem;
