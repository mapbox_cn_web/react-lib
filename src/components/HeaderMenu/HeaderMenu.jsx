import React, { PureComponent, Fragment } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import { PopupContext } from "context";

import CustomModal from "components/CustomModal";

import LinkTo from "components/LinkTo";
import SvgIcon from "components/SvgIcon";
import styles from "./HeaderMenu.module.scss";

class HeaderMenu extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      onAway: this.handleAway,
      path: undefined
    };
  }

  handleEnter = () => {
    this.setState({ open: false });
    window.open(this.state.href, "_blank");
  };

  handleLeave = e => {
    this.setState({ open: false });
  };

  handleAway = href => {
    this.setState({ open: true, href });
  };

  render() {
    const { downloadUrl, activeTab } = this.props;

    console.log(this.state.open);

    return (
      <PopupContext.Provider value={this.state}>
        <div className={styles.root}>
          {this.props.items.map(({ id, name, url }) => {
            if (url) {
              return (
                <LinkTo
                  key={id}
                  href={url}
                  className={classnames(
                    styles.item,
                    id === activeTab ? styles.select : null
                  )}
                >
                  {name}
                </LinkTo>
              );
            }
          })}
          <LinkTo
            href={downloadUrl}
            className={classnames(styles.item, styles.lastItem)}
          >
            <SvgIcon width={17} name="miniDownload" />
            安装
          </LinkTo>
        </div>
        <CustomModal
          open={this.state.open}
          onEnter={this.handleEnter}
          onLeave={this.handleLeave}
        />
      </PopupContext.Provider>
    );
  }
}

HeaderMenu.defaultProps = {
  items: [],
  activeTab: "",
  onClick: () => {}
};

export default HeaderMenu;
