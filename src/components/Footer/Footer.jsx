import React from "react";
import Logo from "components/Logo";
import Animation from "components/Animation";
import Image from "components/Image";
import Hover from "components/Hover";
import Popover from "components/Popover";
import { PopupContext } from "context";
import {
  footerLinks,
  mobileFooterLiks,
  footerCopyrights,
  footerSocial
} from "data/footer";
import platformHoc from "hocs/platformHoc";
import styles from "./Footer.module.scss";

const AnimationWrapper = ({ children, isVisible }) => (
  <Animation
    in={styles.showPopover}
    out={styles.hidePopover}
    duration={{ in: 300, out: 200 }}
    easing="cubic-bezier(0.165, 0.840, 0.440, 1.000)"
    isVisible={isVisible}
  >
    {children}
  </Animation>
);

class Footer extends React.PureComponent {
  state = {
    popup: false,
    pathname: ""
  };

  componentDidMount() {
    this.setState({ pathname: window.location.pathname });
  }

  handleHover = name => isHover => {
    if (isHover && name === "weixin") {
      this.setState({ popup: true });
    } else {
      this.setState({ popup: false });
    }
  };

  handleAway = (onAway, href) => event => {
    event.preventDefault();
    onAway(href);
  };

  render() {
    const { author, title, isMobile } = this.props;
    const { pathname } = this.state;

    const links = isMobile ? mobileFooterLiks : footerLinks;

    return (
      <footer className={styles.root}>
        <div className="container-fluid">
          <div className="row align-items-start justify-content-between">
            <div className={`col-0 col-sm-2 col-md-4 col-lg-3 ${styles.logo}`}>
              <Logo withTitle={false} width={170} />
            </div>
            <div className="col-12 col-sm-10 col-md-8 col-lg-9">
              <ul className={`row justify-content-between ${styles.nav}`}>
                {links.map((item, i) => (
                  <li
                    key={i}
                    className={`${
                      pathname === item.href
                        ? `col-6 col-md-3 ${styles.active}`
                        : "col-6 col-md-3"
                    } ${item.href ? "" : styles.main}`}
                  >
                    {item.href ? (
                      <a href={item.href}>{item.name}</a>
                    ) : (
                      <span>{item.name}</span>
                    )}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row justify-content-between">
            <div className={`col-12 col-md-6 ${styles.copyrigts}`}>
              <span>© Mapbox {new Date().getFullYear()}</span>
              {
                // <Link to={footerCopyrights.privacy.href}>
                //   {footerCopyrights.privacy.name}
                // </Link>
              }
              <PopupContext.Consumer>
                {({ onAway }) => (
                  <a
                    target={footerCopyrights.mapbox.target}
                    href={footerCopyrights.mapbox.href}
                    onClick={this.handleAway(
                      onAway,
                      footerCopyrights.mapbox.href
                    )}
                  >
                    {footerCopyrights.mapbox.name}
                  </a>
                )}
              </PopupContext.Consumer>
            </div>
            <div className={`col-12 col-md-6 ${styles.social}`}>
              {footerSocial.map((item, i) => (
                <Hover onHover={this.handleHover(item.name)} key={i}>
                  <a target="_blank" href={item.href}>
                    <Image path={item.icon} />
                  </a>
                  {item.name === "weixin" && (
                    <AnimationWrapper isVisible={this.state.popup}>
                      <Popover
                        style={{ marginLeft: 12, width: "auto" }}
                        styleWrapper={{ width: 240 }}
                        align="top"
                      >
                        <Image
                          path="socials/qr-weixin.jpg"
                          className={styles.qrCode}
                        />
                      </Popover>
                    </AnimationWrapper>
                  )}
                </Hover>
              ))}
            </div>
          </div>
        </div>
        <hr />
        <div className="container-fluid">
          <div className={styles.address}>{footerCopyrights.address}</div>
        </div>
      </footer>
    );
  }
}

export default platformHoc(Footer);
